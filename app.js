const app = Vue.createApp({
    data() {
        return  {
            
        }
    },
    created(){
        console.log("component instance created");
    },
    methods: {
        
    },
});

app.component('main-navbar', {
    template: `
    <nav class="navbar navbar-expand-sm navbar-light bg-light">
        <a class="navbar-brand" href="#">Blog</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavId">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="#">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Register</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>
    `
})

app.component('register-form', {
    template: `
    <h3 class="mt-4 text-center">Register</h3>
    <div class="row">
        <div class="col-sm-6 mx-auto mt-4">
            <form id="register-form" @submit.prevent="handleSubmit()">
                <div class="form-group">
                    <label for="user_name">Name :</label>
                    <input type="text" class="form-control" name="user_name" id="user_name" v-model="user_name" placeholder="">
                    <small id="emailHelpId" class="form-text text-muted">Enter Name</small>
                </div>

                <div class="form-group">
                    <label for="email">Email :</label>
                    <input type="email" class="form-control" name="email" id="email" v-model="email" placeholder="">
                    <small id="emailHelpId" class="form-text text-muted">Enter email</small>
                </div>
                

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" id="password" v-model="password" placeholder="">
                </div>
                <button type="submit" class="btn btn-primary my-2">Submit</button>
            </form>
        </div>
    </div>
    `,
    data(){
        return {
            user_name : '',
            email : '',
            password : ''
        }
    },
    methods: {
        handleSubmit(){
            console.log("Form submitted");
            console.log(this.user_name, this.email, this.password);
        }
    }
})

// mount return root component instance
// vm stands for view model
const vm = app.mount('#app');
